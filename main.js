//Didn't like having values of 0 at the start, so just changed them to null. 

var app = new Vue({
	el: '#exchange',
	data: {
			ExchangeData: null,
			exg: "SEK",
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: null,
			outputAmount: null,

	},
	created(){

	},
	//This is to get the default value of SEK when you load the page. 
	mounted() {
   		axios 
   		.get('https://api.exchangeratesapi.io/latest?base=SEK')
   		.then(response => (this.ExchangeData = response.data.rates))
   		.catch(error => console.log(error))
  	},
  	methods: {
  		//To change the current currency. this.exg at the bottom is to change the one printed at the top.
  		changeCurrency(currency) { 
  			axios
  			.get('https://api.exchangeratesapi.io/latest?base=' +currency)
	   		.then(response => (this.ExchangeData = response.data.rates))
	   		.catch(error => console.log(error))
	   		this.exg = currency
  		},
  		//Had to put almost everything in the response, otherwise it didn't work when I started changing the currencies. 
  		calcValue() {

  			let fromCurrency = this.selectedFromCurrency
  			console.log('Selected from currency: ' + fromCurrency)

  			axios
  			.get('https://api.exchangeratesapi.io/latest?base=' +fromCurrency)
  			.then(response => {this.ExchangeData = response.data.rates

	  			let toCurrency = this.ExchangeData[this.selectedToCurrency]
	  			console.log('Exchange rate of selected to currency: ' + toCurrency)

	  			let fromValue = this.inputAmount
	  			console.log('Input: ' + fromValue)

	  			console.log('Output: ' + this.outputAmount)

  				let converted = fromValue * toCurrency
  				console.log('Converted: ' + converted)
				this.outputAmount = converted
				this.exg = fromCurrency
  			})
  			.catch(error => console.log(error))
  		}, 

  	}
})


